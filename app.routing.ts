import {Routes} from '@angular/router';
import {EboardMenuComponent} from './src/app/components/base/eboard-menu/eboard-menu.component';
import {EboardRoomComponent} from './src/app/components/base/eboard-room/eboard-room.component';
import {AboutComponent} from './src/app/components/base/about/about.component';
import {SelectRoomMenuComponent} from './src/app/components/common/select-room-menu/select-room-menu.component';

export const AppRoutes: Routes = [
	{
		path: '',
		redirectTo: '/eboard/menu',
		pathMatch: 'full'
	},
	{
		path: 'eboard',
		component: EboardMenuComponent,
		children: [
			{
				path: 'menu',
				component: SelectRoomMenuComponent,
			},
			{
				path: 'about',
				component: AboutComponent,
			}
		]
	},
	{
		path: 'room/:uuid',
		component: EboardRoomComponent
	}
];
