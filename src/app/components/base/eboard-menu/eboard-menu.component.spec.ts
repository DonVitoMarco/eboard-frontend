import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EboardMenuComponent } from './eboard-menu.component';

describe('EboardMenuComponent', () => {
	let component: EboardMenuComponent;
	let fixture: ComponentFixture<EboardMenuComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [EboardMenuComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(EboardMenuComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
