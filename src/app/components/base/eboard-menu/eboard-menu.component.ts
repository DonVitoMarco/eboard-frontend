import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-eboard-menu',
	templateUrl: './eboard-menu.component.html',
	styleUrls: ['./eboard-menu.component.scss']
})
export class EboardMenuComponent implements OnInit {

	constructor(private router: Router) {
	}

	ngOnInit() {
	}

	currentRouteIsMenu(): boolean {
		return this.router.url.includes('menu');
	}

	currentRouteIsAbout(): boolean {
		return this.router.url.includes('about');
	}

}
