import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EboardRoomComponent } from './eboard-room.component';

describe('EboardRoomComponent', () => {
	let component: EboardRoomComponent;
	let fixture: ComponentFixture<EboardRoomComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [EboardRoomComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(EboardRoomComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
