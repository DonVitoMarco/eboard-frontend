import { Note } from './note';

export class Board {

	uuid: string;
	name: string;
	notes: Array<Note>;

	constructor() {
		this.uuid = '';
		this.name = '';
		this.notes = [];
	}

}
