import { Board } from './board';

export class Room {

	uuid: string;
	title: string;
	usersUuid: string;
	boards: Array<Board>;
	createDate: Date;

	constructor() {
		this.uuid = '';
		this.title = '';
		this.usersUuid = '';
		this.boards = [];
	}

}
