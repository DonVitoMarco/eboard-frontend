export class Note {

	uuid: string;
	title: string;
	content: string;
	author: string;
	color: string;
	date: string;
	votes: Array<string>;

	constructor(uuid?: string, title?: string, content?: string, author?: string, color?: string, date?: string, votes?) {
		this.uuid = uuid;
		this.title = title;
		this.content = content;
		this.author = author;
		this.color = color;
		this.date = date;
		this.votes = votes;
	}

}
