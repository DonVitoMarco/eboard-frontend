import { Component, OnInit } from '@angular/core';
import { StompService } from 'ng2-stomp-service';
import { UUID } from 'angular2-uuid';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { Room } from './model/room';
import { Observable } from 'rxjs/Observable';
import { RoomService } from '../../../services/room.service';

@Component({
	selector: 'app-eboard-room',
	templateUrl: './eboard-room.component.html',
	styleUrls: ['./eboard-room.component.scss']
})
export class EboardRoomComponent implements OnInit {

	room: Room = new Room();
	userUuid: string;
	roomUuid: string;

	roomSubscription: Observable<any>;

	constructor(private stomp: StompService, private router: Router, private roomService: RoomService) {
		this.userUuid = UUID.UUID();
		this.roomUuid = this.router.url.substr(this.router.url.lastIndexOf('/') + 1);
		this.roomService.getRoom(this.roomUuid)
			.subscribe(response => this.room = response);

		this.stomp.configure({
			host: environment.serverUrl + environment.stompEndpoint,
			debug: false,
			queue: {'init': false}
		});

		stomp.startConnect().then(() => {
			stomp.done('init');
		});
	}

	ngOnInit() {
		this.stomp.after('init').then(() => {
			this.roomSubscription = this.stomp.subscribe('/topic/room/' + this.roomUuid, response => {
				if (response.action === 'ADD') {
					this.addNote(response);
				} else if (response.action === 'REMOVE') {
					this.removeNote(response);
				} else if (response.action === 'ADD_VOTE') {
					this.addVote(response);
				} else if (response.action === 'REMOVE_VOTE') {
					this.removeVote(response);
				} else if (response.action === 'MOVE') {
					this.moveNote(response);
				} else if (response.action === 'EDIT') {
					this.editNote(response);
				}
			});
		});
	}

	handleNoteUpdated(message: any) {
		this.stomp.send('/app/room/' + this.roomUuid, message);
	}

	addNote(response: any) {
		if (this.isNoteExists(response.note.uuid)) {
			return;
		}
		this.room.boards.forEach(function (elem) {
			if (elem.uuid === response.targetUuid) {
				elem.notes.push(response.note);
			}
		});
	}

	editNote(response: any) {
		if (!this.isNoteExists(response.note.uuid)) {
			return;
		}
		this.removeNote(response);
		this.addNote(response);
	}

	addVote(response: any) {
		if (!this.isNoteExists(response.note.uuid)) {
			return;
		}
		this.room.boards.forEach(function (board) {
			board.notes.forEach(function (note) {
				if (note.uuid === response.note.uuid) {
					note.votes.push(response.userUuid);
				}
			});
		});
	}

	moveNote(response: any) {
		this.removeNote(response);
		this.addNote(response);
	}

	removeVote(response: any) {
		if (!this.isNoteExists(response.note.uuid)) {
			return;
		}
		this.room.boards.forEach(function (board) {
			board.notes.forEach(function (note) {
				note.votes.forEach(function (vote) {
					if (note.uuid === response.note.uuid && vote === response.userUuid) {
						note.votes.splice(note.votes.indexOf(vote), 1);
					}
				});
			});
		});
	}

	removeNote(response: any) {
		if (!this.isNoteExists(response.note.uuid)) {
			return;
		}
		this.room.boards.forEach(function (board) {
			board.notes.forEach(function (note) {
				if (note.uuid === response.note.uuid) {
					board.notes.splice(board.notes.indexOf(note), 1);
				}
			});
		});
	}

	isNoteExists(noteUuid: string): boolean {
		let isExist = false;
		this.room.boards.forEach(function (board) {
			board.notes.forEach(function (boardNote) {
				if (boardNote.uuid === noteUuid) {
					isExist = true;
				}
			});
		});
		return isExist;
	}

	exportRoomUrl() {
		return `${environment.serverUrl}/export/${this.room.uuid}`;
	}

}
