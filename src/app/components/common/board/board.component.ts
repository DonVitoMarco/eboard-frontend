import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Board } from '../../base/eboard-room/model/board';
import { Note } from '../../base/eboard-room/model/note';
import { UUID } from 'angular2-uuid';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { StompService } from 'ng2-stomp-service/index';
import { NoteMessage } from '../../../model/note-message';
import { BoardDialogComponent } from '../board-dialog/board-dialog.component';

@Component({
	selector: 'app-board',
	templateUrl: './board.component.html',
	styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

	@Input()
	boards: Array<Board>;

	@Input()
	userUuid: string;

	@Output()
	noteUpdated = new EventEmitter();

	constructor(private dragulaService: DragulaService, private stomp: StompService, public dialog: MatDialog) {
		dragulaService.drop.subscribe((value) => {
			this.moveNote(value[1].id, value[2].id, value[3].id);
		});
	}

	ngOnInit() {
	}

	openDialog(boardUuid: string, boardName: string, isEdit: boolean, noteContent?: string, noteTitle?: string, noteUuid?: string): void {
		let data = {};
		if (isEdit) {
			data = {boardUuid: boardUuid, boardName: boardName, noteContent: noteContent, noteTitle: noteTitle, edit: true};
		} else {
			data = {boardUuid: boardUuid, boardName: boardName, noteContent: '', noteTitle: '', edit: false};
		}

		let dialogRef = this.dialog.open(BoardDialogComponent, {
			width: '400px',
			height: '280px',
			data: data
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result && !isEdit) {
				this.addNote(result.boardUuid, result.noteTitle, result.noteContent);
			} else if (result && isEdit) {
				this.editNote(noteUuid, result.boardUuid, result.noteTitle, result.noteContent);
			}
		});
	}

	addNote(boardUuid: string, noteTitle: string, noteContent: string) {
		let note: Note = new Note(UUID.UUID(), noteTitle, noteContent, window.localStorage.getItem('username'), '#ffffff', new Date().toLocaleString(), []);
		this.noteUpdated.emit(new NoteMessage('ADD', note, boardUuid, boardUuid, this.userUuid));
	}

	editNote(noteUuid: string, boardUuid: string, newNoteTitle: string, newNoteContent: string) {
		let note: Note = this.findNoteByUuid(noteUuid);
		let updateNote: Note = new Note(note.uuid, newNoteTitle, newNoteContent, window.localStorage.getItem('username'), '#ffffff', new Date().toLocaleString(), note.votes);
		this.noteUpdated.emit(new NoteMessage('EDIT', updateNote, boardUuid, noteUuid, this.userUuid));
	}

	removeNote(boardUuid: string, noteUuid: string) {
		let note: Note = this.findNoteByUuid(noteUuid);
		if (note) {
			this.noteUpdated.emit(new NoteMessage('REMOVE', note, boardUuid, boardUuid, this.userUuid));
		}
	}

	addVoteNote(noteUuid: string) {
		let note: Note = this.findNoteByUuid(noteUuid);
		if (note) {
			this.noteUpdated.emit(new NoteMessage('ADD_VOTE', note, noteUuid, noteUuid, this.userUuid));
		}
	}

	removeVoteNote(noteUuid: string) {
		let note = this.findNoteByUuid(noteUuid);
		if (note) {
			this.noteUpdated.emit(new NoteMessage('REMOVE_VOTE', note, noteUuid, noteUuid, this.userUuid));
		}
	}

	private moveNote(noteUuid, targetUuid, sourceUuid) {
		let note = this.findNoteByUuid(noteUuid);
		if (note) {
			this.noteUpdated.emit(new NoteMessage('MOVE', note, targetUuid, sourceUuid, this.userUuid));
		}
	}

	private findNoteByUuid(noteUuid: string): Note {
		let existNote: Note = null;
		this.boards.forEach(function (board) {
			board.notes.forEach(function (boardNote) {
				if (boardNote.uuid === noteUuid) {
					existNote = boardNote;
				}
			});
		});
		return existNote;
	}

	isUserVoting(noteUuid: string, userUuid: string): boolean {
		let voted: boolean = false;
		this.boards.forEach(function (board) {
			board.notes.forEach(function (note) {
				note.votes.forEach(function (vote) {
					if (noteUuid === note.uuid && vote === userUuid) {
						voted = true;
					}
				});
			});
		});
		return voted;
	}

}
