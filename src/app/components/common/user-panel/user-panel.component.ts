import { Component, HostListener, Input, OnInit } from '@angular/core';
import { User } from '../../base/eboard-room/model/user';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../../../services/user.service';
import { StompService } from 'ng2-stomp-service/index';

@Component({
	selector: 'app-user-panel',
	templateUrl: './user-panel.component.html',
	styleUrls: ['./user-panel.component.scss']
})
export class UserPanelComponent implements OnInit {

	users: Array<User> = [];

	@Input()
	roomUuid: string;

	@Input()
	userUuid: string;

	newUsername = '';

	private usersSubscription: Observable<any>;

	constructor(private stomp: StompService, private userService: UserService) {
	}

	ngOnInit() {
		this.userService
			.addUser(this.roomUuid, this.userUuid)
			.subscribe((response) => {
				this.getInitialUsers();
			});

		this.stomp.after('init').then(() => {
			this.usersSubscription = this.stomp.subscribe('/topic/users/' + this.roomUuid, response => {
				if (response.action === 'ADD') {
					this.addUserEvent(response.user);
				} else if (response.action === 'REMOVE') {
					this.removeUserEvent(response.user);
				} else if (response.action === 'CHANGE') {
					this.changeUsernameEvent(response.user);
				}
			});

			const usernameFromLocalStorage = window.localStorage.getItem('username');
			if (usernameFromLocalStorage) {
				this.changeUsernameDiscreetly(usernameFromLocalStorage);
			} else {
				window.localStorage.setItem('username', 'anonymous');
			}
		});
	}

	private getInitialUsers() {
		this.userService
			.getAllUsers(this.roomUuid)
			.subscribe(response => {
				this.users = response;
			});
	}

	private addUserEvent(user: User) {
		this.users.push(user);
	}

	private removeUserEvent(user: User) {
		this.users = this.users.filter(u => {
			return u.uuid !== user.uuid;
		});
	}

	private changeUsernameEvent(user: User) {
		this.users.forEach((u) => {
			if (u.uuid === user.uuid) {
				u.username = user.username;
			}
		});
	}

	private changeUsernameDiscreetly(changeName: string) {
		if (changeName) {
			const user: User = new User();
			user.uuid = this.userUuid;
			user.username = changeName;
			this.userService
				.changeUsername(this.roomUuid, this.userUuid, user)
				.subscribe(() => {
					window.localStorage.setItem('username', user.username);
				});
		}
	}

	changeUsername($event) {
		if (this.newUsername !== '') {
			const user: User = new User();
			user.uuid = this.userUuid;
			user.username = this.newUsername;
			this.userService
				.changeUsername(this.roomUuid, this.userUuid, user)
				.subscribe(() => {
					this.newUsername = '';
					window.localStorage.setItem('username', user.username);
				});
		}
	}

	@HostListener('window:beforeunload', ['$event'])
	beforeUnloadHandler($event) {
		const xhr = new XMLHttpRequest();
		xhr.open('DELETE', `http://localhost:8080/api/room/${this.roomUuid}/user/${this.userUuid}`);
		xhr.send();
		return true;
	}

}
