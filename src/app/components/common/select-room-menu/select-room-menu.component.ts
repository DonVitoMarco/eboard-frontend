import { Component, OnInit } from '@angular/core';
import { RoomMenuEntry } from '../../../model/room-menu-entry';
import { BoardMenuEntry } from '../../../model/board-menu-entry';
import { CreateRoomService } from '../../../services/create-room.service';
import { CreateRoomRequest } from '../../../model/create-room-request';
import { MENU_ROOM_ENTRIES, MenuFactoryService } from '../../../services/menu-factory.service';
import { Room } from '../../base/eboard-room/model/room';
import { RoomService } from '../../../services/room.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-select-room-menu',
	templateUrl: './select-room-menu.component.html',
	styleUrls: ['./select-room-menu.component.scss']
})
export class SelectRoomMenuComponent implements OnInit {

	title: string;
	boards: Array<BoardMenuEntry> = [];
	showImportInput: boolean;

	roomMenuEntries: Array<RoomMenuEntry>;
	selectedRoom: RoomMenuEntry;

	constructor(private createRoomService: CreateRoomService,
				private menuFactoryService: MenuFactoryService,
				private roomService: RoomService,
				private router: Router) {
	}

	ngOnInit() {
		this.title = 'EBOARD';
		this.roomMenuEntries = MENU_ROOM_ENTRIES;
		this.selectedRoom = this.roomMenuEntries[2];
	}

	chooseRoom() {
		this.boards = this.menuFactoryService.boardsEntriesFactory(this.selectedRoom);
	}

	createBoard() {
		this.createRoomService.createRoom(new CreateRoomRequest(this.title, this.boards));
	}

	addBoard() {
		this.boards.push(new BoardMenuEntry('new eboard'));
	}

	deleteBoard() {
		this.boards.pop();
	}

	backToMenu() {
		this.boards = [];
	}

	importRoom($event) {
		const reader = new FileReader();
		if ($event.target.files && $event.target.files.length > 0) {
			const file = $event.target.files[0];
			reader.readAsText(file);
			reader.onload = () => {
				const room: Room = JSON.parse(reader.result);
				this.roomService.importRoom(room).subscribe((response) => {
					this.router.navigateByUrl('/room/' + response.uuid);
				});
			};
		}
	}

	toggleShow() {
		this.showImportInput = !this.showImportInput;
	}

}
