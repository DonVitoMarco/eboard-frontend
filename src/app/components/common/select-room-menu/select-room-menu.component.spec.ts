import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectRoomMenuComponent } from './select-room-menu.component';

describe('SelectRoomMenuComponent', () => {
	let component: SelectRoomMenuComponent;
	let fixture: ComponentFixture<SelectRoomMenuComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SelectRoomMenuComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SelectRoomMenuComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
