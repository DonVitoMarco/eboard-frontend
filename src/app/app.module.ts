import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StompService } from 'ng2-stomp-service';

import { AppComponent } from './app.component';
import { AboutComponent } from './components/base/about/about.component';
import { BoardComponent } from './components/common/board/board.component';
import { CreateRoomService } from './services/create-room.service';
import { EboardMenuComponent } from './components/base/eboard-menu/eboard-menu.component';
import { EboardRoomComponent } from './components/base/eboard-room/eboard-room.component';
import { SelectRoomMenuComponent } from './components/common/select-room-menu/select-room-menu.component';
import { MenuFactoryService } from './services/menu-factory.service';
import { UserPanelComponent } from './components/common/user-panel/user-panel.component';
import { RoomService } from './services/room.service';
import { UserService } from './services/user.service';
import { DragulaModule } from 'ng2-dragula';
import { MatButtonModule, MatDialogModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { BoardDialogComponent } from './components/common/board-dialog/board-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutes } from '../../app.routing';

@NgModule({
	declarations: [
		AboutComponent,
		AppComponent,
		BoardComponent,
		BoardDialogComponent,
		EboardMenuComponent,
		EboardRoomComponent,
		SelectRoomMenuComponent,
		UserPanelComponent
	],
	imports: [
		AngularFontAwesomeModule,
		BrowserModule,
		BrowserAnimationsModule,
		DragulaModule,
		FormsModule,
		HttpModule,
		HttpClientModule,
		MatFormFieldModule,
		MatDialogModule,
		MatInputModule,
		MatButtonModule,
		RouterModule.forRoot(AppRoutes, { useHash: true })
	],
	providers: [
		CreateRoomService,
		MenuFactoryService,
		RoomService,
		StompService,
		UserService
	],
	bootstrap: [
		AppComponent
	],
	entryComponents: [
		BoardDialogComponent
	]
})
export class AppModule {
}
