import { BoardMenuEntry } from './board-menu-entry';

export class CreateRoomRequest {

	title: string;
	boardsList: Array<BoardMenuEntry>;

	constructor(title?: string, boardsList?: Array<BoardMenuEntry>) {
		this.title = title;
		this.boardsList = boardsList;
	}

}
