import { Note } from '../components/base/eboard-room/model/note';

export class NoteMessage {

	action: string;
	note: Note;
	targetUuid: string;
	sourceUuid: string;
	userUuid: string;

	constructor(action: string, note: Note, targetUuid: string, sourceUuid: string, userUuid: string) {
		this.action = action;
		this.note = note;
		this.targetUuid = targetUuid;
		this.sourceUuid = sourceUuid;
		this.userUuid = userUuid;
	}

}
