export class CreateRoomResponse {

	uuid: string;

	constructor(uuid: string) {
		this.uuid = uuid;
	}

}
