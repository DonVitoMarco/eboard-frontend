import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../components/base/eboard-room/model/user';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ResponseString } from '../model/response-string';

@Injectable()
export class UserService {

	serverUrl: string;

	constructor(private http: Http, private httpClient: HttpClient) {
		this.serverUrl = environment.serverUrl;
	}

	addUser(roomUuid: string, userUuid: string): Observable<User> {
		return this.httpClient
			.post<User>(`${this.serverUrl}/api/room/${roomUuid}/user/${userUuid}`, {});
	}

	removeUser(roomUuid: string, userUuid: string) {
		return this.http
			.delete(`${this.serverUrl}/api/room/${roomUuid}/user/${userUuid}`)
			.map((res) => res.json());
	}

	getAllUsers(roomUuid: string): Observable<User[]> {
		return this.httpClient
			.get<User[]>(`${this.serverUrl}/api/room/${roomUuid}/users`);
	}

	changeUsername(roomUuid: string, userUuid: string, user: User) {
		return this.httpClient
			.put<ResponseString>(`${this.serverUrl}/api/room/${roomUuid}/user/${userUuid}`, user);
	}

}
