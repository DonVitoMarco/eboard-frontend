import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Room } from '../components/base/eboard-room/model/room';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { CreateRoomResponse } from '../model/create-room-response';

@Injectable()
export class RoomService {

	private serverUrl: string;

	constructor(private httpClient: HttpClient) {
		this.serverUrl = environment.serverUrl;
	}

	getRoom(roomUuid: string): Observable<Room> {
		return this.httpClient
			.get<Room>(`${this.serverUrl}/api/room/${roomUuid}`);
	}

	importRoom(room: Room): Observable<CreateRoomResponse> {
		return this.httpClient.post<CreateRoomResponse>(`${this.serverUrl}/import`, room);
	}

}
