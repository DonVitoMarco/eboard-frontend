import { Injectable } from '@angular/core';
import { RoomMenuEntry } from '../model/room-menu-entry';
import { BoardMenuEntry } from '../model/board-menu-entry';

@Injectable()
export class MenuFactoryService {

	constructor() {
	}

	boardsEntriesFactory(selectedRoom: RoomMenuEntry) {
		const boards: Array<BoardMenuEntry> = [];
		switch (selectedRoom.name) {
			case 'single':
				boards.push(new BoardMenuEntry('my eboard'));
				break;
			case 'double':
				boards.push(new BoardMenuEntry('to do'));
				boards.push(new BoardMenuEntry('done'));
				break;
			case 'retrospective':
				boards.push(new BoardMenuEntry('positive'));
				boards.push(new BoardMenuEntry('negative'));
				boards.push(new BoardMenuEntry('actions'));
				break;
			case 'kanban board':
				boards.push(new BoardMenuEntry('to do'));
				boards.push(new BoardMenuEntry('in progress'));
				boards.push(new BoardMenuEntry('done'));
				break;
			case 'scrum board':
				boards.push(new BoardMenuEntry('to do'));
				boards.push(new BoardMenuEntry('in progress'));
				boards.push(new BoardMenuEntry('done'));
				break;
			case 'other':
				boards.push(new BoardMenuEntry('new eboard'));
				break;
		}
		return boards;
	}

}

export const MENU_ROOM_ENTRIES = [
	new RoomMenuEntry('single', 1),
	new RoomMenuEntry('double', 2),
	new RoomMenuEntry('retrospective', 3),
	new RoomMenuEntry('kanban board', 4),
	new RoomMenuEntry('scrum board', 5),
	new RoomMenuEntry('other', 0),
];
