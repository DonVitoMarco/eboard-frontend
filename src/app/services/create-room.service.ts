import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { CreateRoomRequest } from '../model/create-room-request';
import { HttpClient } from '@angular/common/http';
import { CreateRoomResponse } from '../model/create-room-response';
import { environment } from '../../environments/environment';

@Injectable()
export class CreateRoomService {

	serverUrl: string;

	constructor(private httpClient: HttpClient, private router: Router) {
		this.serverUrl = environment.serverUrl;
	}

	createRoom(rooms: CreateRoomRequest): void {
		this.httpClient
			.post<CreateRoomResponse>(`${this.serverUrl}/api/room`, rooms)
			.subscribe(response => {
				this.router.navigateByUrl('/room/' + response.uuid);
			});
	}

}
